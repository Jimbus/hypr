#!/bin/bash

if [ -z $1 ] || [ ! -d $1 ] 
then
	echo -e "Usage: \033[1m$(basename -- $0)\033[0m \033[4mwallpapers dir\033"
	exit 1
fi

if pidof -x $(basename -- $0) -o $$ > /dev/null 
then
	echo -e "\033[1m$(basename -- $0) is already Running...\033"
	exit 2
fi

if pidof -x "swaybg" > /dev/null
then
	killall swaybg
fi

wallpapersDir=$1

generate_wallpapers_array () {
	unset wallpapers
	readarray -d '' wallpapers < <(find "$wallpapersDir" -type f -print0)
}

set_new_wallpaper () {
	if [ ${#wallpapers[@]} -le 0 ]
	then
		generate_wallpapers_array
	fi

	rndIndex=$(($RANDOM % ${#wallpapers[@]})) # Generate a random number within the range of the wallpapers array length.
	newWallpaper=${wallpapers[$rndIndex]} # Set the new wallpaper variable to the randomly selected image.
	unset wallpapers[rndIndex] # Remove the randomly selected image from the array.
	wallpapers=("${wallpapers[@]}") # Recreate the array without gaps.

	if [ ! -e "$newWallpaper" ]
	then
		exit 3
	fi

	killall swaybg
	$(dirname -- ${BASH_SOURCE[0]})/swaybg -i "$newWallpaper" -m multi-monitor </dev/null &>/dev/null &

	if [ "$?" -ne 0 ] 
	then
		exit 4
	fi
}

trap set_new_wallpaper SIGUSR1
generate_wallpapers_array

while true
do
	fullscreenFlag=$(hyprctl -j clients | jq 'map(.fullscreen==true) | any')	

	if ! $fullscreenFlag
	then
		set_new_wallpaper
	fi
	
	sleep 300 &
	sleepPID=$!
	while ps -p $sleepPID > /dev/null
	do
		wait $sleepPID	
	done
done

