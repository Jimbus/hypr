#!/bin/bash

start_gpu_screen_recorder () {
  if pidof -x "gpu-screen-recorder" -o $$ > /dev/null
  then
    send_notification "gpu-screen-recorder is already running..."
    exit 3
  else
    send_notification "Replay recording started..."
    flatpak run --command=gpu-screen-recorder com.dec05eba.gpu_screen_recorder -w DP-1 -a "$(pactl get-default-sink).monitor" -f 60 -r 30 -q ultra -cr full -c mkv -o ~/Videos &
  fi
}

stop_gpu_screen_recorder () {
  if pidof -x "gpu-screen-recorder" -o $$ > /dev/null
  then
    send_notification "gpu-screen-recorder has been stopped, replay saving is no longer possible!"
    killall -SIGINT gpu-screen-recorder
  else
    exit 4
  fi
}

save_replay () {
  if pidof -x "gpu-screen-recorder" -o $$ > /dev/null
  then
    send_notification "Replay recording saved..."
    killall -SIGUSR1 gpu-screen-recorder
  else
    send_notification "Unable to save the replay! gpu-screen-recorder is not running!"
    exit 2
  fi
}

print_usage () {
  echo -e "Usage: \033[1m$(basename -- $0)\033[0m [start] [stop] [toggle] [save]"
}

send_notification () {
  notify-send 'gpu-screen-recorder' "$1" --icon=$(dirname -- ${BASH_SOURCE[0]})/com.dec05eba.gpu_screen_recorder.png
}

if [ -z $1 ]
then
  print_usage
  exit 1
fi

command="$1"

case $command in
  start)
    start_gpu_screen_recorder
    ;;
  stop)
    stop_gpu_screen_recorder
    ;;
  toggle)
    if pidof -x "gpu-screen-recorder" -o $$ > /dev/null
    then
      stop_gpu_screen_recorder
    else
      start_gpu_screen_recorder
    fi
    ;;
  save)
    save_replay
    ;;
  *)
    print_usage
    ;;
esac

exit $?
