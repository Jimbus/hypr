#!/bin/bash

chosen=$(echo -e "Shutdown\nReboot\nSuspend" | $1)

if [[ $chosen = "Shutdown" ]]; then
	systemctl poweroff -i
elif [[ $chosen = "Reboot" ]]; then
	systemctl reboot -i
elif [[ $chosen = "Suspend" ]]; then
	sleep 1 && systemctl suspend
fi
