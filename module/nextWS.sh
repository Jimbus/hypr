#!/bin/bash

maxWorkspace=5
curOutput=$(hyprctl -j monitors | jq -r 'map(.focused == true) | index(true)')
curWorkspace=$(hyprctl -j monitors | jq -r '.. | try select(.focused == true).activeWorkspace.id')

if [ $1 == "prev" ]
then
	nextWorkspace=$((($curWorkspace % $maxWorkspace) - 1))
	if [ $nextWorkspace -eq 0 ]
	then
		nextWorkspace=$maxWorkspace
	elif [ $nextWorkspace -lt 0 ]
	then
		nextWorkspace=$(($maxWorkspace - 1))
	fi
else
	nextWorkspace=$((($curWorkspace % $maxWorkspace) + 1))
fi

nextWorkspace=$(($nextWorkspace + ($maxWorkspace * ($curOutput))))

hyprctl dispatch workspace $nextWorkspace
