#!/bin/bash

pid=$(hyprctl -j clients | jq '.[] | select(.focusHistoryID==0).pid')

kill -9 $pid
