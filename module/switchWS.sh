#!/bin/bash

if [ -z $1 ] || [ $1 -lt 1 ]
then
	exit 1
fi

maxWorkspace=5
curOutput=$(hyprctl -j monitors | jq -r 'map(.focused == true) | index(true)')

arg=""
workspace=$(($1 + ($maxWorkspace * ($curOutput))))

if [ $2 == "move" ]
then
	arg="dispatch movetoworkspacesilent"
else
	arg="dispatch workspace"
fi

hyprctl $arg $workspace
