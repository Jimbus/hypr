#!/bin/bash

TEST=$(ps x | awk '$5 ~ /wlsunset/ {print $5}')
if [ -z $TEST ]
then
	wlsunset -T 4050 &
else
	pkill wlsunset
fi

