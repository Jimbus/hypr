#!/bin/bash
CPU_TEMP=$(( $(cat /sys/class/hwmon/hwmon5/temp1_input) / 1000 ))
GPU_TEMP=$(( $(cat /sys/class/drm/card1/device/hwmon/hwmon1/temp1_input) / 1000 ))

echo "{\"text\":\""$GPU_TEMP \ $CPU_TEMP"\", \"tooltip\":\""GPU - CPU"\"}"
