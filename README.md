# Hyprland

This project aims to reproduce my [swayWM configuration](https://gitlab.com/Jimbus/sway).

![Preview](./examples/desktop.png)

## Installation
`cd` to your `.config` folder then:
```bash
git clone git@gitlab.com:Jimbus/hypr.git
```
or
```bash
git clone https://gitlab.com/Jimbus/hypr.git
```

## Dependencies
### Standard dependencies:

- [wayland](https://wiki.archlinux.org/title/Wayland) Communication protocol that specifies the communication between a display server and its clients.

- [lib32-wayland](https://wiki.archlinux.org/title/Wayland) [Optional] Communication protocol that specifies the communication between a display server and its clients.

- [hyprland](https://github.com/hyprwm/Hyprland) Hyprland is a highly customizable dynamic tiling Wayland compositor that doesn't sacrifice on its looks.

- [xorg-xwayland](https://wiki.archlinux.org/title/Wayland#XWayland) [Optional] X Clients under Wayland.

- [swayidle](https://wiki.archlinux.org/title/Sway#Idle) [Optional] Idle manager for Wayland.

- [xdg-desktop-portal-hyprland](https://github.com/hyprwm/xdg-desktop-portal-hyprland) [Optional] xdg-desktop-portal backend for hyprland. 

- [qt5-wayland](https://wiki.qt.io/QtWayland) [Optional] QtWayland is a Qt 5 module that wraps the functionality of Wayland.

- [qt6-wayland](https://wiki.qt.io/QtWayland) [Optional] QtWayland is a Qt 6 module that wraps the functionality of Wayland.

### Script and shortcuts dependencies:

- [waybar](https://github.com/Alexays/Waybar) Highly customizable Wayland bar for Sway and Wlroots based compositors.

- [rofi](https://github.com/lbonn/rofi) A window switcher, run dialog and dmenu replacement - fork with wayland support  

- [grim](https://github.com/emersion/grim) Grab images from a Wayland compositor.

- [slurp](https://github.com/emersion/slurp) Select a region in a Wayland compositor and print it to the standard output. Works well with grim.

- [wlsunset](https://sr.ht/~kennylevinsen/wlsunset/) Day/night gamma adjustments for Wayland compositors supporting wlr-gamma-control-unstable-v1.

- [mako](https://github.com/emersion/mako) Is a lightweight notification daemon for Wayland compositors that support the layer-shell protocol.

- [polkit-gnome](https://gitlab.gnome.org/Archive/policykit-gnome) For elevating programs that need root access.

- [cliphist](https://github.com/sentriz/cliphist) Wayland clipboard manager.

- [wl-clip-persist](https://github.com/Linus789/wl-clip-persist) Keep Wayland clipboard even after programs close.

- [playerctl](https://github.com/altdesktop/playerctl) for media button control.

- [foot](https://codeberg.org/dnkl/foot) The fast, lightweight and minimalistic Wayland terminal emulator.

- [blueman](https://github.com/blueman-project/blueman) a GTK+ Bluetooth Manager.

- [udiskie](https://github.com/coldfix/udiskie) udisks2 front-end that allows to manage removable media such as CDs or flash drives from userspace.

- [network-manager-applet](https://gitlab.gnome.org/GNOME/network-manager-applet) Tray applet and an advanced network connection editor.

- [eww](https://github.com/elkowar/eww) Elkowars Wacky Widgets is a standalone widget system made in Rust that allows you to implement your own, custom widgets in any window manager.

- [satty](https://github.com/gabm/satty) Satty is a screenshot annotation tool inspired by Swappy and Flameshot.

- [gpu-screen-recorder](https://git.dec05eba.com/gpu-screen-recorder/about/) This is a screen recorder that has minimal impact on system performance by recording your monitor using the GPU only, similar to shadowplay on windows. This is the fastest screen recording tool for Linux.
