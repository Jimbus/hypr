#/bin/bash

isPowerMenuShown=$(eww -c ~/.config/hypr/eww get isPowerMenuShown)
focussedMonitor=$(hyprctl -j monitors | jq -r '.[0] | try select(.focused == true)')

if [ $isPowerMenuShown == "false" ]
then
  eww -c ~/.config/hypr/eww close powerMenu

  if [ -z "$focussedMonitor" ]
  then 
    eww -c ~/.config/hypr/eww open powerMenu --screen 1
  else 
    eww -c ~/.config/hypr/eww open powerMenu --screen 0
  fi

  eww -c ~/.config/hypr/eww update isPowerMenuShown=true
else
  eww -c ~/.config/hypr/eww update isPowerMenuShown=false
fi
